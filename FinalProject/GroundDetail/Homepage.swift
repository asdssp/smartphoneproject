//
//  Homepage.swift
//  FinalProject
//
//  Created by kpugame on 2021/06/03.
//

import SwiftUI
import WebKit

struct Homepage: UIViewRepresentable{
    var urlToLoad: String
    
    func makeUIView(context: Context) -> some UIView {
        guard let url = URL(string: self.urlToLoad) else {
            let webView = WKWebView()
            return webView
        }
        print("로드 성공")
        let webView = WKWebView()
        webView.load(URLRequest(url:url))
        return webView
        
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
}
