//
//  GroundHostingController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/21.
//

import UIKit
import SwiftUI
class GroundHostingController: UIHostingController<GroundDetailSwiftUI> {
    var OtherData : [StardiumDetailData]?
    var DetailData: StardiumDetailData?
    
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder, rootView: GroundDetailSwiftUI(data: nil, count: nil))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var count :[Int] = [0,0,0,0,0,0,0,0,0,0,0]
        for i in 0..<OtherData!.count {
            var num = Int(Int(OtherData![i].hresum as String)! / 10)
            if(num > 10){
                num = 10
            }
            count[num] += 1
        }
        count[Int(Int(DetailData!.hresum as String)! / 10)] += 1
        rootView.data = DetailData!
        rootView.count = count
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      if segue.identifier == "segueToGroundMap" {
        /*
            if let mapController = segue.destination as? MapViewController {
                mapController.MapPoints.append(MapAnnotation(title: DetailData!.bsshNm as String, locationName: DetailData!.bsshAddr2 as String, discipline: DetailData!.homepage as String, coordinate: DetailData!.location))
                
                mapController.initLocationCoord = DetailData!.location
                
                
                for i in 0..<OtherData!.count{
                    mapController.MapPoints.append(MapAnnotation(title: OtherData![i].bsshNm as String, locationName: OtherData![i].bsshAddr2 as String, discipline: OtherData![i].homepage as String, coordinate: OtherData![i].location))
                }
            }
          */
        if let autoMapController = segue.destination as? AutoMapViewController {
           
            autoMapController.title = DetailData?.bsshNm as String?
            /*
            autoMapController.addr = DetailData?.bsshAddr1 as String?
            autoMapController.name = DetailData?.bsshNm as String?
            autoMapController.discipline = DetailData?.homepage as String?
            */
            autoMapController.simpleMapData = SimpleMapData(name: (DetailData?.bsshAddr1 as String?)!, addr: (DetailData?.bsshNm as String?)!, discipline: (DetailData?.homepage as String?)!)
            
            
            }
        
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
