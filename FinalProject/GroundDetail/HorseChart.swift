//
//  HorseChart.swift
//  FinalProject
//
//  Created by kpugame on 2021/06/03.
//

import SwiftUI

struct HorseChart: View {
    var horseCount:[Int]
    var mainCount : Int
    var body: some View {
        VStack{
            Text("평균 경마장 마필 개수(X10)")
                .underline()
            HStack{
                ForEach(0..<11) { count in
                    VStack{
                        Spacer()
                        Text("\(self.horseCount[count])")
                            .font(.footnote)
                            .rotationEffect(.degrees(-90))
                            .offset(y: self.horseCount[count] < 15 ? 0 : 35)
                            .zIndex(1)
                        
                        Rectangle()
                            .fill(count == mainCount ? Color.red : Color.blue)
                            .frame(width: 20, height: horseCount[count] < 75 ? CGFloat( self.horseCount[count] * 2) : 150)
                    
                        Text("\(count)")
                            .font(.footnote)
                            .foregroundColor(count == mainCount ? .red : .black)
                            .frame(height:20)
                    }
                }
            }
        }
    }
}

struct HorseChart_Previews: PreviewProvider {
    static var previews: some View {
        HorseChart(horseCount : [0,1,2,3,4,5,6,7,8,9,10], mainCount: 1)
    }
}
