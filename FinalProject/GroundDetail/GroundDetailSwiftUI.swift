//
//  GroundDetailSwiftUI.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/21.
//

import SwiftUI


struct FamilyRow: View{
    var str: String
    
    var body: some View{
        HStack{
            Text(str)
        }
    }
}

struct GroundDetailSwiftUI: View {
    var data :StardiumDetailData?
    var count : [Int]?
    var body: some View {
       
        if data != nil && count != nil{
            VStack{
                List {
                    FamilyRow(str: "사업자명 : \(data!.bsshNm as String)")
                    FamilyRow(str: "사업장 주소 : \(data!.bsshAddr1 as String)")
                    FamilyRow(str: "대표자명 : \(data!.rprsntvNm as String)")
                    FamilyRow(str: "대표자 전화번호 : \(data!.rprsntvPhone as String)")
                    FamilyRow(str: "홈페이지 주소 : \(data!.homepage as String)")
                    FamilyRow(str: "운영 시간 : \(data!.operationTime as String)")
                    FamilyRow(str: "승마자 소개글 : \(data!.hrareaintrcnsntnc as String)")
                }
                TabView{
                    HorseChart(horseCount: count!, mainCount: Int(Int(data!.hresum as String)! / 10) > 10 ? 10: Int(Int(data!.hresum as String)! / 10)).tabItem({
                        Image(systemName: "doc")
                        Text("HorseCount")
                    })
                    Homepage(urlToLoad: data!.homepage as String).tabItem({
                        Image(systemName: "link")
                        Text("Homepage")
                    })
                }
            }
        }
    }
}
