//
//  XMLData.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/20.
//

import Foundation
import MapKit
struct SearchHorseData
{
    
    let birthDt : NSString // 승용마 출생 일자
    let breed : NSString // 승용마 품종
    let color : NSString // 승용마 털 색
    let hrName : NSString // 승용마 이름
    let hrNo : NSString // 승용마 고유 번호
    let owNm : NSString // 승용마 소유자명
    let pggDt : NSString // 승용마 혈통 등록 일자
    let poNm : NSString // 승용마 현 소재지
    let prdCty : NSString // 승용마 생산 국가 명
    let sex : NSString // 승용마 성별
}


struct SearchHorseDetailData
{
    let hrName : NSString // 승용마 이름
    let hrNo : NSString // 승용마 번호
    let birthDt : NSString // 승용마 생일
    let sex : NSString // 승용마 털 색
    let color : NSString // 승용마 털색
    let breed : NSString // 승용마 품종
    let pggDt : NSString // 혈통 등록일
    let prdCty : NSString // 생산국
    let fhrNm : NSString // 부마
    let mhrNm : NSString // 모마
    let owNm : NSString // 소유자
    let poNm : NSString // 소재지
    
}

struct StardiumData{
    let addr:NSString // 승마장 소재지 상세주소
    let areaNm:NSString // 승마장 소재지 지역 구분
    let ridingNm:NSString // 승마장 명칭
}

struct StardiumDetailData{
    let hraSeq:NSString // 승마장 일련번호
    let bsshNm:NSString // 사업자명
    let bsshAddr1:NSString // 사업장 주소
    let bsshAddr2:NSString // 사업장 세부 주소
    let rprsntvNm:NSString // 대표자명
    let rprsntvEmail:NSString // 대표자 이메일
    let rprsntvPhone:NSString // 대표자 전화번호
    let rstde:NSString // 휴무일
    let homepage:NSString // 홈페이지 주소
    let operationTime:NSString // 운영 시간
    let bizregnum:NSString // 사업자 등록 번호
    let hrareatynm:NSString // 승마장 유형
    let corprHrridingFcltyTyText:NSString // 협력승마시설 유형
    let reprsntcoach:NSString // 대표 코치
    let hresum:NSString // 마필보유두수
    let edcProgrmText:NSString // 교육프로그램
    let ptrProgrmText:NSString // 참여프로그램
    let insrexprndt:NSString //보험만료일
    let hrareaintrcnsntnc:NSString //승마장 소개글
    let staffnm:NSString // 실무자명
}




