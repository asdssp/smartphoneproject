//
//  XMLParser.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/20.
//

import Foundation
class XMLParserClass : NSObject, XMLParserDelegate{
  
    // xml파일을 다운로드 및 파싱하는 오브젝트
    var parser = XMLParser()
    // feed 데이터를 저장하는 mutable array
    var posts = NSMutableArray()
    var HigherCategory:String = ""
    var Categorys:[String] = []
    
    var elements = NSMutableDictionary()
    var element = NSString()
    var data : Dictionary<String, NSMutableString> = Dictionary()
    
    func getPosts() -> NSMutableArray{
        return posts
    }
    

    func getCount()->Int{
        return posts.count
    }
    
    func getCategoryData(category:String, index:Int) -> NSString{
        if !Categorys.contains(category)
        {
            return ""
        }
        let value = (posts.object(at: index) as AnyObject).value(forKey: category) as! NSString
        return value
    }
    
    func getCategoryData(category:String)->[NSString]{
        var returnValue: [NSString] = []
        for i in 0..<posts.count {
            let value = (posts.object(at: i) as AnyObject).value(forKey: category) as! NSString
            returnValue.append(value)
        }
        return returnValue
    }
    
    
    func setHigherCategory(category:String)
    {
        HigherCategory = category
    }
    
    func setCategory(categorys:[String])
    {
        Categorys = categorys
    }
    
    func addCategory(category:String)
    {
        Categorys.append(category)
    }
    
    func beginParsing(xmlURL:String, Key:String, Option:String){
        posts = []
        let accessURL:String = xmlURL + "?ServiceKey=" + Key + Option
        parser = XMLParser(contentsOf:(URL(string: accessURL))!)!
        parser.delegate = self
        parser.parse()
    }
    
    func parser(_ parser:XMLParser, didStartElement elementName:String, namespaceURI:String?, qualifiedName qName:String?, attributes attributeDict:[String: String]){
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item")
        {
            elements = NSMutableDictionary()
            elements = [:]
            
            data = [:]
            for category in Categorys{
                data[category] = NSMutableString()
                data[category] = ""
            }
        }
    }
    
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        for category in Categorys{
            if element.isEqual(to: category)
            {
                data[category]?.append(string)
            }
        }
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if(elementName as NSString).isEqual(to: HigherCategory){
            for category in Categorys{
                if let categoryData = data[category]{
                    if !categoryData.isEqual(nil){
                        elements.setObject(categoryData, forKey: category as NSCopying)
                    }
                }
            }
            posts.add(elements)
        }
    }
    
    
}
