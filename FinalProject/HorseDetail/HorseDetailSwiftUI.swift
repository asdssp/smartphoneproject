//
//  GroundDetailSwiftUI.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/21.
//

import SwiftUI

struct HorseDetailSwiftUI: View {
    var selectedData : SearchHorseDetailData?
    var selectedColorCount : Int
    var selectedGreedCount : Int
    var GreedCount : [Int]
    var Greed : [String]
    var ColorCount : [Int]
    var Color : [String]
    var body: some View {
        if selectedData != nil {
            List {
                FamilyRow(str: "승용마 이름 : \(selectedData!.hrName as String)")
                FamilyRow(str: "번호 : \(selectedData!.hrNo as String)")
                FamilyRow(str: "생일 : \(selectedData!.birthDt as String)")
                FamilyRow(str: "털색 : \(selectedData!.color as String)")
                FamilyRow(str: "성별 : \(selectedData!.sex as String)")
                FamilyRow(str: "소재지 : \(selectedData!.poNm as String)")
                FamilyRow(str: "소유자 : \(selectedData!.owNm as String)")
            }
            TabView{
                GreedChart(GreedCount: GreedCount, Greed: Greed, mainCount: selectedGreedCount).tabItem({
                    Image(systemName: "chart.bar")
                    Text("Greed")
                })
                ColorChart(colorCount: ColorCount, color: Color, mainIndex: selectedColorCount).tabItem({
                    Image(systemName: "chart.bar")
                    Text("Color")
                })
            }
        }
    }
}
