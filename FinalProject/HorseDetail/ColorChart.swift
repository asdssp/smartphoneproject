//
//  ColorChart.swift
//  FinalProject
//
//  Created by kpugame on 2021/06/03.
//

import SwiftUI

struct ColorChart: View {
    var colorCount : [Int]
    var color : [String]
    var mainIndex : Int
    var body: some View {
        VStack{
            Text("색깔에 따른 승마말 수")
                .underline()
            Spacer()
            VStack{
                ForEach(0..<8) { count in
                    HStack{
            
                        Text("\(color[count])")
                            .font(.footnote)
                            .foregroundColor(count == mainIndex ? .red : .black)
                            .frame(width:100)

                        Rectangle()
                            .fill(count == mainIndex ? Color.red : Color.blue)
                            .frame(width: colorCount[count] < 75 ? CGFloat( self.colorCount[count] * 2) : 150, height: 20)
                    
                        Text("\(self.colorCount[count])")
                            .font(.footnote)
                            .offset(x: self.colorCount[count] < 15 ? 0 : 35)
                            .zIndex(1)
                        
                        Spacer()
                    }
                }
            }.padding()
        }

    }
}

