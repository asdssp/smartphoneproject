//
//  GreedChart.swift
//  FinalProject
//
//  Created by kpugame on 2021/06/03.
//

import SwiftUI

struct GreedChart: View {
    var GreedCount : [Int]
    var Greed : [String]
    var mainCount : Int
    var body: some View {
        VStack{
            Text("품종에 따른 승마말 수")
                .underline()
            HStack{
                ForEach(0..<6) { count in
                    VStack{
                        Spacer()
                        Text("\(self.GreedCount[count])")
                            .font(.footnote)
                            .rotationEffect(.degrees(-90))
                            .offset(y: self.GreedCount[count] < 15 ? 0 : 35)
                            .zIndex(1)
                        
                        Rectangle()
                            .fill(count == mainCount ? Color.red : Color.blue)
                            .frame(width: 20, height: GreedCount[count] < 75 ? CGFloat( self.GreedCount[count] * 2) : 150)
                    
                        Text("\(Greed[count])")
                            .font(.footnote)
                            .foregroundColor(count == mainCount ? .red : .black)
                            .frame(height:20)
                    }
                }
            }
        }

    }
}
