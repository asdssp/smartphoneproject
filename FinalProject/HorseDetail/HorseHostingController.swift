//
//  GroundHostingController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/21.
//

import UIKit
import SwiftUI
class HorseHostingController: UIHostingController<HorseDetailSwiftUI> {
    var otherDatas : [SearchHorseDetailData]?
    var detailData: SearchHorseDetailData?
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder, rootView: HorseDetailSwiftUI(selectedData: nil, selectedColorCount: 0, selectedGreedCount: 0, GreedCount: [], Greed: [], ColorCount: [], Color: []))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var ColorCount = [0,0,0,0,0,0,0,0]
        var GreedCount = [0,0,0,0,0,0]
        rootView.selectedData = detailData
        
        if let other = otherDatas
        {
            for data in other{
                switch(data.color)
                {
                case "밤색":
                    ColorCount[0] += 1
                    break
                case "팔로미노":
                    ColorCount[1] += 1
                    break
                case "갈색":
                    ColorCount[2] += 1
                    break
                case "회갈색":
                    ColorCount[3] += 1
                    break
                case "회색":
                    ColorCount[4] += 1
                    break
                case "흑갈색":
                    ColorCount[5] += 1
                    break
                case "검은색":
                    ColorCount[6] += 1
                    break
                default:
                    ColorCount[7] += 1
                    break
                }
                
                switch(data.breed){
                case "하프링거":
                    GreedCount[0] += 1
                    break
                case "하노버":
                    GreedCount[1] += 1
                    break
                case "코네마라":
                    GreedCount[2] += 1
                    break
                case "쿼터호스":
                    GreedCount[3] += 1
                    break
                case "소형마":
                    GreedCount[4] += 1
                    break
                default:
                    GreedCount[5] += 1
                    break
                }
            }
        }
        
        switch(detailData!.color)
        {
        case "밤색":
            ColorCount[0] += 1
            rootView.selectedColorCount = 0
            break
        case "팔로미노":
            ColorCount[1] += 1
            rootView.selectedColorCount = 1
            break
        case "갈색":
            ColorCount[2] += 1
            rootView.selectedColorCount = 2
            break
        case "회갈색":
            ColorCount[3] += 1
            rootView.selectedColorCount = 3
            break
        case "회색":
            ColorCount[4] += 1
            rootView.selectedColorCount = 4
            break
        case "흑갈색":
            ColorCount[5] += 1
            rootView.selectedColorCount = 5
            break
        case "검은색":
            ColorCount[6] += 1
            rootView.selectedColorCount = 6
            break
        default:
            ColorCount[7] += 1
            rootView.selectedColorCount = 7
            break
        }
        
        switch(detailData!.breed){
        case "하프링거":
            GreedCount[0] += 1
            rootView.selectedGreedCount = 0
            break
        case "하노버":
            GreedCount[1] += 1
            rootView.selectedGreedCount = 1
            break
        case "코네마라":
            GreedCount[2] += 1
            rootView.selectedGreedCount = 2
            break
        case "쿼터호스":
            GreedCount[3] += 1
            rootView.selectedGreedCount = 3
            break
        case "소형마":
            GreedCount[4] += 1
            rootView.selectedGreedCount = 4
            break
        default:
            GreedCount[5] += 1
            rootView.selectedGreedCount = 5
            break
        }
        
        rootView.Greed = ["하프링거", "하노버", "코네마라", "쿼터호스", "소형마", "기타"]
        rootView.GreedCount = GreedCount
        rootView.Color = ["밤색", "팔로미노", "갈색", "회갈색", "회색", "흑갈색", "검은색", "기타"]
        rootView.ColorCount = ColorCount
        
        // Do any additional setup after loading the view.
    }
    
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


