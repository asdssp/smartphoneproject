//
//  MapGetLocation.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/20.
//

import Foundation
import CoreLocation

class GetLocater{
    var location:CLLocationCoordinate2D = CLLocationCoordinate2D()
    var place: String
    init(place: String){
        self.place = place
    }
    
    func getCoordinatesFromPlace(place: String){
            let geoCoder = CLGeocoder()
        
            geoCoder.geocodeAddressString(place) { (placemarks, error) in
                guard
                    let placemarks = placemarks,
                    let location = placemarks.first?.location?.coordinate
                else {
                    // handle no location found
                    print("no location")
                    return
                }
                
                self.location = location
            }
        }
    
    func getLatitude() -> CLLocationDegrees {
        self.getCoordinatesFromPlace(place: place)
        return self.location.latitude
    }
    
    func getLongitude() -> CLLocationDegrees {
        self.getCoordinatesFromPlace(place: place)
        return self.location.longitude
    }
    
    func getLocationCoord2D() ->CLLocationCoordinate2D{
        self.getCoordinatesFromPlace(place: place)
        return location
    }
}
