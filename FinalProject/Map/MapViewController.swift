//
//  MapViewController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/20.
//

import Foundation
import MapKit


class MapViewController : UIViewController{
    // MapView 링크
    @IBOutlet weak var mapView: MKMapView!
    
    // 지도 범위 크기
    let regionRadius:CLLocationDistance = 1000
    
    // 주요 좌표
    var MapPoints: [MapAnnotation] = []
    var initLocationCoord :CLLocationCoordinate2D?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let initLocation = CLLocation(latitude: initLocationCoord!.latitude, longitude: initLocationCoord!.longitude)
        
        centerMapOnLocation(location: initLocation)
        mapView.delegate = self
    }
    
    // 해당 좌표 중앙으로 이동
    func centerMapOnLocation(location: CLLocation){
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    
    
    
}


extension MapViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? MapAnnotation else {return nil}
        
        let identifer = "marker"
        var view:MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifer) as? MKMarkerAnnotationView{
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else{
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifer)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view:MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        let location = view.annotation as! MapAnnotation
        let launchOptions = [MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    
    
}
