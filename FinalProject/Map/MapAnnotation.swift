//
//  MapAnnotation.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/20.
//

import Foundation
import MapKit
import Contacts
class MapAnnotation: NSObject, MKAnnotation{
    
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    
    init(title:String, locationName:String, discipline:String, coordinate:CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
    
    func mapItem() -> MKMapItem{
        let addrDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary:  addrDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
    
}

