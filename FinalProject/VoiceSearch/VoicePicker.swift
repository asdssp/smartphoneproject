//
//  VoiceSearcher.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/21.
//

import Foundation
import UIKit
import Speech

class UIPickerTranscriber{
    init(button:UIButton, pickerView:UIPickerView){
        transcribeButton = button
        self.pickerView = pickerView
    }
    
    var transcribeButton:UIButton
    var recordResult:String = ""
    var pickerView : UIPickerView
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "ko-KR"))!
    private var speechRecognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var speechRecognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    func startTranscribing(){
        try! startSession()
    }
    
    func stopTranscribing(){
        if audioEngine.isRunning {
            audioEngine.stop()
            speechRecognitionRequest?.endAudio()
        }
        
        switch (recordResult){
        // 항목 추가
        case "sample" : self.pickerView.selectRow(0, inComponent: 0, animated: true)
            break
        default:
            break
        }
    }
    
    func startSession(){
        if let recognitionTask = speechRecognitionTask{
            recognitionTask.cancel()
            self.speechRecognitionTask = nil
        }
        let audioSession = AVAudioSession.sharedInstance()
        try! audioSession.setCategory(AVAudioSession.Category.record)
        
        speechRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let recognitionRequset = speechRecognitionRequest else
        { fatalError("SFSpeechAudioBufferRecognitionRequset object creation failed")}
        
        let inputNode = audioEngine.inputNode
        
        recognitionRequset.shouldReportPartialResults = true
        
        speechRecognitionTask = speechRecognizer.recognitionTask(with: recognitionRequset) {result, error in
            var finished = false
            if let result = result{
                self.recordResult = result.bestTranscription.formattedString
                finished = result.isFinal
            }
            if error != nil || finished{
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.speechRecognitionRequest = nil
                self.speechRecognitionTask = nil
                self.transcribeButton.isEnabled = true
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer:AVAudioPCMBuffer, when:AVAudioTime) in
    
            self.speechRecognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        try! audioEngine.start()
    }
    
    
    func authorizeSR(){
        SFSpeechRecognizer.requestAuthorization {authStatus in
            OperationQueue.main.addOperation {
                switch authStatus{
                case .authorized:
                    self.transcribeButton.isEnabled = true
                    
                case .denied:
                    self.transcribeButton.isEnabled = false
                    self.transcribeButton.setTitle("Speech recognition access denied by user", for: .disabled)
                    
                case .restricted:
                    self.transcribeButton.isEnabled = false
                    self.transcribeButton.setTitle("Speech recognition restricted on device", for: .disabled)
                    
                case .notDetermined:
                    self.transcribeButton.isEnabled = false
                    self.transcribeButton.setTitle("Speech recognition not authorized", for: .disabled)
    
                }
            }
            
        }
    }
    
}
