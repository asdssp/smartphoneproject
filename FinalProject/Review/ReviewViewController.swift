//
//  ReviewViewController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/21.
//

import UIKit

//첫번째 리뷰 페이지
class ReviewViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, UITableViewDelegate
{
    /* XML 관련 변수 - viewController의 prepare 함수에서 세팅 */
    // xml 주소
    var url: String?
    // xml 서비스 키
    var serviceKey: String?
    // xml 구조 내 상위 카테고리
    var HigherCategory: String?
    // xml 구조 내 카테고리들
    var Categorys: [String]?
    /* EOG */
    
    /* XML 관련 변수 - 검색기 및 검색 옵션 */
    var XMLParserManager = XMLParserClass()
    // 검색 페이지
    var pageNo: Int = 1
    // 페이지 당 결과 개수
    var numOfRows: Int = 500
    /* EOG */
    
    //pickerview 데이터 배열
    var pickerDataSource = ["All" ,"강원", "경기", "광주", "대구", "세종", "울산", "인천", "기타"]
    var searchCategory = ["강원", "경기", "광주", "대구", "세종", "울산", "인천"]
    var fillteredRidingGround = [RidingGround]()
    var ridingGround = [RidingGround]()
    //리뷰 TableView 링크
    @IBOutlet weak var reviewTableView: UITableView!
    //리뷰 PickerView 링크
    @IBOutlet weak var reviewPickerView: UIPickerView!
    
    var pickerViewCurrentRow: String = "All"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reviewPickerView.delegate = self;
        self.reviewPickerView.dataSource = self;
        
        // 카테고리 설정
        XMLParserManager.setHigherCategory(category: HigherCategory!)
        XMLParserManager.setCategory(categorys: Categorys!)
        
        // 파싱 시작
        XMLParserManager.beginParsing(xmlURL: url!, Key: serviceKey!, Option: "&numOfRows=" + String(numOfRows) + "&pageNo=" + String(pageNo))
        
        for i in 0..<XMLParserManager.getCount(){
            let bsshNm = XMLParserManager.getCategoryData(category: "bsshNm", index: i)
            let bsshAddr1 = XMLParserManager.getCategoryData(category: "bsshAddr1", index: i)
            ridingGround.append(RidingGround(name: bsshNm as String, wayPoint: bsshAddr1 as String))
        }
        
        if(ridingGround.count == 0)
        {
            ridingGround.append(RidingGround(name: "대전복용승마장", wayPoint: "대전광역시 유성구 덕명로56번길 199"))
        }
        
        ridingGround.sort(by: {$0.wayPoint < $1.wayPoint})
        
    }
    
    func isFiltering() -> Bool {
         return pickerViewCurrentRow != "All"
    }
    
    func checkEtc(ground: RidingGround) -> Bool
    {
        for i in 0..<searchCategory.count{
            if ground.wayPoint.contains(searchCategory[i])
            {
                return false
            }
        }
        return true
    }
    
    //TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
          return fillteredRidingGround.count
        }
        return ridingGround.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let ground: RidingGround
        if isFiltering() {
          ground = fillteredRidingGround[indexPath.row]
        } else {
          ground = ridingGround[indexPath.row]
        }
        cell.textLabel!.text = ground.name
        cell.detailTextLabel!.text = ground.wayPoint
        return cell
    }
    
    //PickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerViewCurrentRow = pickerDataSource[row]
        
        fillteredRidingGround = ridingGround.filter({( ground : RidingGround) -> Bool in
          let doesCategoryMatch = (pickerViewCurrentRow == "All") || (ground.wayPoint.contains(pickerViewCurrentRow)) || (pickerViewCurrentRow == "기타" && checkEtc(ground: ground))
            
            return doesCategoryMatch
        })
        
        reviewTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToDetailReview" {
            if let navController = segue.destination as? UINavigationController {
                if let topViewController = navController.topViewController as? DetailReviewTableViewController{
                    let cell = sender as! UITableViewCell
                    let indexPath = reviewTableView.indexPath(for: cell)
                    let ground: RidingGround
                    if isFiltering() {
                        ground = fillteredRidingGround[indexPath!.row]
                    } else {
                        ground = ridingGround[indexPath!.row]
                    }
                    topViewController.tag = ground.name
                }
            }
        }
    }
}
