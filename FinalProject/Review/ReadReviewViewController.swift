//
//  ReadReviewViewController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/31.
//

import UIKit

class ReadReviewViewController: UIViewController {

    var review: ReviewData?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingStarsImageView: UIImageView!
    @IBOutlet weak var detailReviewLabel: UILabel!
    func imageForRating(rating: Int) -> UIImage?
    {
        let imageName = "\(rating)Stars"
        return UIImage(named: imageName)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text! = review!.title!
        nameLabel.text! = review!.name!
        ratingStarsImageView.image = imageForRating(rating: review!.Rating)
        detailReviewLabel.text! = review!.detailReview!
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
