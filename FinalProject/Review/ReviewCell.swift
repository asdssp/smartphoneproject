//
//  ReviewCell.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/31.
//

import UIKit

class ReviewCell: UITableViewCell {

    //타이틀 라벨 링크
    @IBOutlet weak var titleLabel: UILabel!
    //이름 라벨 링크
    @IBOutlet weak var nameLabel: UILabel!
    //별 이미지 뷰 링크
    @IBOutlet weak var ratingImageVIew: UIImageView!
    var review:ReviewData! {
        didSet
        {
            titleLabel.text = review.title
            nameLabel.text = review.name
            ratingImageVIew.image = imageForRating(rating: review.Rating)
            
        }
    }
    
    func imageForRating(rating: Int) -> UIImage?
    {
        let imageName = "\(rating)Stars"
        return UIImage(named: imageName)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
