//
//  DetailReviewViewController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/21.
//

import UIKit

var reviews:[ReviewData] = []
var reviewDict = Dictionary<String, [ReviewData]>()
//세부 리뷰 페이지
class DetailReviewTableViewController: UITableViewController
{
    //세부 리뷰 tableView 링크
    @IBOutlet var detailReviewTableView: UITableView!
    var tag:String?
    
    
    override func viewDidLoad() {
        if reviewDict[tag!] == nil{
            reviewDict[tag!] = [ReviewData]()
        }
        super.viewDidLoad()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewDict[tag!]!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
        
        let review = reviewDict[tag!]![indexPath.row] as ReviewData
        cell.review = review
        
        return cell
    }
    
    @IBAction func saveToDetailReview(segue: UIStoryboardSegue)
    {
        if let AddReviewViewController = segue.source as? AddReviewViewController
        {
            if let review = AddReviewViewController.reviews
            {
                reviewDict[tag!]!.append(review)
            }
        }
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ReadReviewViewController
        {
            if let indexPath = tableView.indexPathForSelectedRow {
            let vc = segue.destination as? ReadReviewViewController
                vc?.review = reviewDict[tag!]![indexPath.row]
            }
        }
    }
}
