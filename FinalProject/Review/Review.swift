//
//  Review.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/22.
//

import UIKit

//리뷰 정보 구조체
struct ReviewData
{
    //제목
    var title: String?
    //작성자 이름
    var name: String?
    //별정
    var Rating: Int
    //세부 리뷰글
    var detailReview: String?
    
    init(title: String?, name: String?, Rating: Int, detailReview: String?)
    {
        self.title = title
        self.name = name
        self.Rating = Rating
        self.detailReview = detailReview
    }
}
