//
//  AddReviewViewController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/21.
//

import UIKit

//리뷰 등록 페이지
class AddReviewViewController: UITableViewController
{
    //제목 tableFeild 링크
    @IBOutlet weak var titleTextFeild: UITextField!
    //작성자 이름 tableFeild 링크
    @IBOutlet weak var nameTextFeild: UITextField!
    //별 이미지 뷰 링크
    @IBOutlet weak var ratingImgaeView: UIImageView!
    func imageForRating(rating: Int) -> UIImage?
    {
        let imageName = "\(rating)Stars"
        return UIImage(named: imageName)
    }
    //세부 평가 tableView 링크
    @IBOutlet weak var detailTextView: UITextView!
    //리뷰 정보들
    var reviews: ReviewData?
    //평가 정보
    var rating: Int = 1
    {
        didSet
        {
            ratingImgaeView.image? = imageForRating(rating: rating)!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ratingImgaeView.image = imageForRating(rating: rating)
    }
    
    @IBAction func unwindWithSelectedStar(segue: UIStoryboardSegue)
    {
        if let starPickerViewController = segue.source as? RatingStarsTableViewController
        {
            rating = starPickerViewController.rating
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "SaveDetailReview"
        {
            reviews = ReviewData(title: titleTextFeild.text!, name: nameTextFeild.text!, Rating: rating, detailReview: detailTextView.text!)
        }
    }
}
