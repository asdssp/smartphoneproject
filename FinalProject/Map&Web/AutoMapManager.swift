//
//  AutoMap.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/23.
//

import Foundation
import UIKit
import MapKit
import WebKit
import CoreLocation
class AutoMapManager : NSObject{
    var parentView: UIView
    var convertButton :UIBarButtonItem
    var convert:Bool = false
    let regionRadius = 1000
    var title:String?
    var place:String?
    var discipline:String?
    
    var mapView:MKMapView?
    var webView:WKWebView?
    
    init(parentView: UIView, convertButton:UIBarButtonItem){
        self.parentView = parentView
        self.convertButton = convertButton
    }
    
    func LoadAutoMap(title: String, place: String, discipline:String){
        self.title = title
        self.place = place
        self.discipline = discipline
        
            let geoCoder = CLGeocoder()
                self.convertButton.isEnabled = true
        geoCoder.geocodeAddressString(place) { [self] (placemarks, error) in
                guard
                    let placemarks = placemarks,
                    let location = placemarks.first?.location?.coordinate
                else {
                    // handle no location found, so make webView and load GoogleMap
                    print("handle no location found, so make webView and load GoogleMap")
                    
                    loadGoogleMap(title: title)
                
                    self.convertButton.isEnabled = false
                    return
                }
            
                // handle location found, so make mapView and set coordinate
                print("handle location found, so make mapView and set coordinate")
            
            if mapView == nil {
                mapView = MKMapView(frame: CGRect(x: 0, y: 0, width: (parentView.window?.frame.width)!, height: (parentView.window?.frame.height)!))
                mapView?.delegate = self
                self.parentView.addSubview(mapView!)
            }
            else
            {
                mapView?.isHidden = false
            }
                let coordinateRegion = MKCoordinateRegion(center: location, latitudinalMeters: CLLocationDistance(self.regionRadius), longitudinalMeters: CLLocationDistance(self.regionRadius))
                mapView?.setRegion(coordinateRegion, animated: true)
            mapView?.addAnnotation(MapAnnotation(title: title, locationName: place, discipline: discipline, coordinate: location))
                convert = false
            }
        }
    
    func addAnnotationWithSimpleData(simpleMapData: SimpleMapData)
    {
        let geoCoder = CLGeocoder()
            self.convertButton.isEnabled = true
        geoCoder.geocodeAddressString(simpleMapData.addr) { [self] (placemarks, error) in
            guard
                let placemarks = placemarks,
                let location = placemarks.first?.location?.coordinate
            else {
                // handle no location found, so make webView and load GoogleMap
                print("need more Delay to use GeoCoder")
                return
            }
            mapView?.addAnnotation(MapAnnotation(title: simpleMapData.name, locationName: simpleMapData.addr, discipline: simpleMapData.discipline, coordinate: location))
        }
    }
    
    
    func convertMap(){
        //self.parentView.subviews.forEach({$0.removeFromSuperview()})
        if(convert){
            webView?.isHidden = true
            LoadAutoMap(title: self.title!, place: self.place!, discipline: self.discipline!)
        }
        else{
            mapView?.isHidden = true
            loadGoogleMap(title: self.title!)
        }
    }
    
    func loadGoogleMap(title:String){
        let urlOriginString = "https://www.google.com/maps/search/?api=1&query=" + title
        let urlString = urlOriginString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: urlString!) else
        {
            print("faile to load googleMap")
            return
        }
        let request = URLRequest(url : url)
        
        if webView == nil{
            webView = WKWebView(frame: CGRect(x: 0, y: 0, width: (parentView.window?.frame.width)!, height: (parentView.window?.frame.height)!))
        }
        else{
            webView?.isHidden = false
        }
        self.parentView.addSubview(webView!)
        webView?.load(request)
        self.convert = true
    }
}

extension AutoMapManager : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? MapAnnotation else {return nil}
        
        let identifer = "marker"
        var view:MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifer) as? MKMarkerAnnotationView{
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else{
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifer)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view:MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        let location = view.annotation as! MapAnnotation
        let launchOptions = [MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    
    
}
