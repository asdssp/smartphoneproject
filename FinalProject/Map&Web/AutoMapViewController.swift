//
//  AutoMapViewController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/23.
//

import UIKit


struct SimpleMapData{
    var name:String
    var addr:String
    var discipline:String
}

class AutoMapViewController: ViewController {
    var simpleMapData:SimpleMapData?
    var autoMap: AutoMapManager?
    
    @IBOutlet weak var ConvertButton: UIBarButtonItem!
    @IBOutlet weak var uiView: UIView!
    
    var loadCount:Int = 0
    
    override func viewDidLoad() {
        loadCount = 0
        autoMap = AutoMapManager(parentView: self.uiView,convertButton: self.ConvertButton)
        autoMap?.LoadAutoMap(title: simpleMapData!.name, place: simpleMapData!.addr, discipline: simpleMapData!.discipline)
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    

    

    @IBAction func ConvertButtonPressed(_ sender: Any) {
        autoMap?.convertMap()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
