//
//  StardustView.swift
//  Anagrams
//
//  Created by KIMYOUNG SIK on 2018. 2. 17..
//  Copyright © 2018년 Caroline. All rights reserved.
//
import UIKit
//ExplodeView와 매우 비슷하다.
class GrassFallView: UIView {
    private var emitter: CAEmitterLayer!
    
    override class var layerClass: AnyClass {
        return CAEmitterLayer.self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("use init(frame: ")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        emitter = self.layer as! CAEmitterLayer
        emitter.emitterPosition = CGPoint(x: self.bounds.size.width / 2, y: 0)
        emitter.emitterSize = CGSize(width: self.bounds.size.width, height: 0)
        emitter.renderMode = CAEmitterLayerRenderMode.additive
        emitter.emitterShape = .line
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if self.superview == nil {
            return
        }
        
        let texture: UIImage? = UIImage(named: "leaf.png")
        assert(texture != nil, "particle image not found")
        
        let emitterCell = CAEmitterCell()
        
        emitterCell.name = "cell"
        emitterCell.contents = texture?.cgImage
        emitterCell.birthRate = 3
        emitterCell.lifetime = 3
        emitterCell.yAcceleration = 100
        emitterCell.scale = 0.025
        emitterCell.scaleRange = 0.05
        emitterCell.spin = 1
        emitterCell.spinRange = CGFloat.pi
        emitterCell.alphaSpeed = -0.15
        emitter.emitterCells = [emitterCell]
        
    }
}

