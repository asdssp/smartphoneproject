//
//  RidingGroundViewController.swift
//  SmartPhoneTeamProject
//
//  Created by kpugame on 2021/05/20.
//

import UIKit

class RidingHorseViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var voiceButton: UIButton!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var HorseImage: UIImageView!
    /* XML 관련 변수 - viewController의 prepare 함수에서 세팅 */
    // xml 주소
    var url: String?
    // xml 서비스 키
    var serviceKey: String?
    // xml 구조 내 상위 카테고리
    var HigherCategory: String?
    // xml 구조 내 카테고리들
    var Categorys: [String]?
    /* EOG */
    
    /* XML 관련 변수 - 검색기 및 검색 옵션 */
    var XMLParserManager = XMLParserClass()
    // 검색 페이지
    var pageNo: Int = 1
    // 페이지 당 결과 개수
    var numOfRows: Int = 500
    /* EOG */
    
    /* 음성 인식 */
    var voiceSearch : UISearchTranscriber?
    var isRecording : Bool = false
    
    var audioController: AudioController
    required init?(coder aDecoder:NSCoder){
        audioController = AudioController()
        audioController.preloadAudioEffects(audioFileNames: [SFX_BUTTON, SFX_HORSE])
        super.init(coder: aDecoder)
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    var fillteredRidingHorse = [SearchHorseDetailData]()
    var ridingHorse = [SearchHorseDetailData]()
    var searchCategory = [String]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        HorseImage.center = CGPoint(x: 250, y: 800)
        // 카테고리 설정
        XMLParserManager.setHigherCategory(category: HigherCategory!)
        XMLParserManager.setCategory(categorys: Categorys!)
        
        // 파싱 시작
        XMLParserManager.beginParsing(xmlURL: url!, Key: serviceKey!, Option: "&numOfRows=" + String(numOfRows) + "&pageNo=" + String(pageNo))
        // 일부 데이터 추출
        for i in 0..<XMLParserManager.getCount(){
            let hrName = XMLParserManager.getCategoryData(category: "hrName", index: i)
            let hrNo = XMLParserManager.getCategoryData(category: "hrNo", index: i)
            let birthDt = XMLParserManager.getCategoryData(category: "birthDt", index: i)
            let sex = XMLParserManager.getCategoryData(category: "sex", index: i)
            let color = XMLParserManager.getCategoryData(category: "color", index: i)
            let breed = XMLParserManager.getCategoryData(category: "breed", index: i)
            let pggDt = XMLParserManager.getCategoryData(category: "pggDt", index: i)
            let prdCty = XMLParserManager.getCategoryData(category: "prdCty", index: i)
            let fhrNm = XMLParserManager.getCategoryData(category: "fhrNm", index: i)
            let mhrNm = XMLParserManager.getCategoryData(category: "mhrNm", index: i)
            let owNm = XMLParserManager.getCategoryData(category: "owNm", index: i)
            let poNm = XMLParserManager.getCategoryData(category: "poNm", index: i)
       
            ridingHorse.append(SearchHorseDetailData(hrName: hrName, hrNo: hrNo, birthDt: birthDt, sex: sex, color: color, breed: breed, pggDt: pggDt, prdCty: prdCty, fhrNm: fhrNm, mhrNm: mhrNm, owNm: owNm, poNm: poNm))
        }
        
        if(ridingHorse.count == 0)
        {
            ridingHorse.append(SearchHorseDetailData(hrName: "찰리브라운", hrNo: "500151", birthDt: "1996-02-03", sex: "거", color: "밤색", breed: "라인란더", pggDt: "2010-03-19", prdCty: "독일", fhrNm: "Champus", mhrNm: "Bianka", owNm: "황현찬", poNm: "HHT승마클럽"))
        }
        
        
        ridingHorse.sort(by: {String($0.hrName) < String($1.hrName)})
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search RidingHorse"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        searchCategory = ["밤색", "팔로미노", "갈색", "회갈색", "회색", "흑갈색", "검은색"]
        searchController.searchBar.scopeButtonTitles = ["All", "밤색", "팔로미노", "갈색", "회갈색", "회색", "흑갈색", "검은색", "기타"]
        
   
        
        searchController.searchBar.delegate = self
        /* 보이스 검색 객체 설정*/
        voiceSearch = UISearchTranscriber(button: voiceButton, searchController: searchController,tableView: tableView)
       
    }
    
    @IBAction func doneTo(seuge:UIStoryboardSegue){
    }
    
    func checkEtc(horse: SearchHorseDetailData) -> Bool
    {
        for i in 0..<searchCategory.count{
            if String(horse.color).contains(searchCategory[i])
            {
                return false
            }
        }
        return true
    }
    
    
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
          return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if isFiltering() {
        return fillteredRidingHorse.count
      }
        
      return ridingHorse.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
      let horse: SearchHorseDetailData
      if isFiltering() {
        horse = fillteredRidingHorse[indexPath.row]
      } else {
        horse = ridingHorse[indexPath.row]
      }
        cell.textLabel!.text = String(horse.hrName)
        cell.detailTextLabel!.text = String(horse.breed)
      return cell
    }
    
    // MARK: - Private instance methods
      
    func searchBarIsEmpty() -> Bool
    {
      // Returns true if the text is empty or nil
      return searchController.searchBar.text?.isEmpty ?? true
    }
      
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
      fillteredRidingHorse = ridingHorse.filter({( horse : SearchHorseDetailData) -> Bool in
        let doesCategoryMatch = (scope == "All") || (String(horse.color) == scope) || (scope == "기타" && checkEtc(horse: horse))
          
        if searchBarIsEmpty() {
          return doesCategoryMatch
        } else {
          return doesCategoryMatch && String(horse.hrName).lowercased().contains(searchText.lowercased())
        }
      })
      tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      if segue.identifier == "segueToHorseDetail" {
        if let indexPath = tableView.indexPathForSelectedRow {
          //let candy = candies[indexPath.row]
          
          let horse : SearchHorseDetailData
          if isFiltering()
          {
            horse = fillteredRidingHorse[indexPath.row]
          }
          else
          {
            horse = ridingHorse[indexPath.row]
          }
          
            if let controller = segue.destination as? HorseHostingController
          {
                // 변수 설정
                controller.detailData = horse
                var otherData: [SearchHorseDetailData]
                if isFiltering()
                {
                    otherData = fillteredRidingHorse
                }
                else
                {
                    otherData = ridingHorse
                }
                otherData.remove(at: indexPath.row)
                controller.otherDatas = otherData
          }
        }
      }
    }
    

    @IBAction func touchVoiceButton(_ sender: Any) {
        if isRecording {
            searchController.isActive = true
            voiceSearch?.stopTranscribing()
            voiceButton.setTitle("음성 검색 On", for: UIControl.State.normal)
            self.isRecording = false
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut,
                           animations: { [self] in
                            HorseImage.center =  CGPoint(x: HorseImage.center.x + 300, y: HorseImage.center.y)
                           },
                           completion: nil)
        }
        else{
            searchController.isActive = true
            voiceSearch?.startTranscribing()
            voiceButton.setTitle("음성 검색 Off", for: UIControl.State.normal)
            self.isRecording = true
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut,
                           animations: { [self] in
                            HorseImage.center =  CGPoint(x: HorseImage.center.x - 300, y: HorseImage.center.y)
                           },
                           completion: nil)
        }
        
        audioController.playerEffect(name: SFX_BUTTON)
    }
}

extension RidingHorseViewController: UISearchResultsUpdating {
  // MARK: - UISearchResultsUpdating Delegate
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
      let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
      filterContentForSearchText(searchController.searchBar.text!, scope: scope)
  }
}

extension RidingHorseViewController: UISearchBarDelegate {
  // MARK: - UISearchBar Delegate
  func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
  }
}
