//
//  RidingGroundViewController.swift
//  SmartPhoneTeamProject
//
//  Created by kpugame on 2021/05/20.
//

import UIKit
import MapKit
class RidingGroundViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var voiceButton: UIButton!
    @IBOutlet var tableView: UITableView!
    // @IBOutlet var searchFooter: SearchFooter!
    
    /* XML 관련 변수 - viewController의 prepare 함수에서 세팅 */
    // xml 주소
    var url: String?
    // xml 서비스 키
    var serviceKey: String?
    // xml 구조 내 상위 카테고리
    var HigherCategory: String?
    // xml 구조 내 카테고리들
    var Categorys: [String]?
    /* EOG */
    
    /* XML 관련 변수 - 검색기 및 검색 옵션 */
    var XMLParserManager = XMLParserClass()
    // 검색 페이지
    var pageNo: Int = 1
    // 페이지 당 결과 개수
    var numOfRows: Int = 500
    /* EOG */
    
    @IBOutlet weak var HorseImage: UIImageView!
    
    /* 음성 인식 */
    var voiceSearch : UISearchTranscriber?
    var isRecording : Bool = false
    
    var audioController: AudioController
    required init?(coder aDecoder:NSCoder){
        audioController = AudioController()
        audioController.preloadAudioEffects(audioFileNames: [SFX_BUTTON, SFX_HORSE])
        super.init(coder: aDecoder)
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    var fillteredRidingGround = [StardiumDetailData]()
    var ridingGround = [StardiumDetailData]()
    var searchCategory = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // 카테고리 설정
        HorseImage.center = CGPoint(x: 250, y: 800)
     
        XMLParserManager.setHigherCategory(category: HigherCategory!)
        XMLParserManager.setCategory(categorys: Categorys!)
        
        // 파싱 시작
        XMLParserManager.beginParsing(xmlURL: url!, Key: serviceKey!, Option: "&numOfRows=" + String(numOfRows)     + "&pageNo=" + String(pageNo))
        
        // 일부 데이터 추출
        for i in 0..<XMLParserManager.getCount(){
            let hraSeq = XMLParserManager.getCategoryData(category: "hraSeq", index: i)
            let bsshNm = XMLParserManager.getCategoryData(category: "bsshNm", index: i)
            let bsshAddr1 = XMLParserManager.getCategoryData(category: "bsshAddr1", index: i)
            let bsshAddr2 = XMLParserManager.getCategoryData(category: "bsshAddr2", index: i)
            let rprsntvNm = XMLParserManager.getCategoryData(category: "rprsntvNm", index: i)
            let rprsntvEmail = XMLParserManager.getCategoryData(category: "rprsntvEmail", index: i)
            let rprsntvPhone = XMLParserManager.getCategoryData(category: "rprsntvPhone", index: i)
            let rstde = XMLParserManager.getCategoryData(category: "rstde", index: i)
            let homepage = XMLParserManager.getCategoryData(category: "homepage", index: i)
            let operationTime = XMLParserManager.getCategoryData(category: "operationTime", index: i)
            let bizregnum = XMLParserManager.getCategoryData(category: "bizregnum", index: i)
            let hrareatynm = XMLParserManager.getCategoryData(category: "hrareatynm", index: i)
            let corprHrridingFcltyTyText = XMLParserManager.getCategoryData(category: "corprHrridingFcltyTyText", index: i)
            let reprsntcoach = XMLParserManager.getCategoryData(category: "reprsntcoach", index: i)
            let hresum = XMLParserManager.getCategoryData(category: "hresum", index: i)
            let edcProgrmText = XMLParserManager.getCategoryData(category: "edcProgrmText", index: i)
            let ptrProgrmText = XMLParserManager.getCategoryData(category: "ptrProgrmText", index: i)
            let insrexprndt = XMLParserManager.getCategoryData(category: "insrexprndt", index: i)
            let hrareaintrcnsntnc = XMLParserManager.getCategoryData(category: "hrareaintrcnsntnc", index: i)
            let staffnm = XMLParserManager.getCategoryData(category: "staffnm", index: i)
            
          // let GeoLocation = GetLocater(place: String(bsshAddr1))
          // let location = GeoLocation.getLocationCoord2D()
        
            ridingGround.append(StardiumDetailData(hraSeq: hraSeq, bsshNm: bsshNm, bsshAddr1: bsshAddr1, bsshAddr2: bsshAddr2, rprsntvNm: rprsntvNm, rprsntvEmail: rprsntvEmail, rprsntvPhone: rprsntvPhone, rstde: rstde, homepage: homepage, operationTime: operationTime, bizregnum: bizregnum, hrareatynm: hrareatynm, corprHrridingFcltyTyText: corprHrridingFcltyTyText, reprsntcoach: reprsntcoach, hresum: hresum, edcProgrmText: edcProgrmText, ptrProgrmText: ptrProgrmText, insrexprndt: insrexprndt, hrareaintrcnsntnc: hrareaintrcnsntnc, staffnm: staffnm))
        }
        
        /* 테스트용 값 입력*/
        if(ridingGround.count == 0)
        {
            ridingGround.append(StardiumDetailData(hraSeq: "1060", bsshNm: "대전복용승마장", bsshAddr1: "대전광역시 유성구 덕명로 56번길 199", bsshAddr2: "복용승마장 (덕명동)", rprsntvNm: "최도경", rprsntvEmail: "goldshoe1@hanmail.net", rprsntvPhone: "042-610-2982", rstde: "월", homepage: "http://www.djsiseol.or.kr", operationTime: "06:00~21:00", bizregnum: "314-82-07247", hrareatynm: "체시법", corprHrridingFcltyTyText: "그린승마존", reprsntcoach: "설동승", hresum: "45", edcProgrmText: "승마체험, 승마강습, 재활승마", ptrProgrmText: "전국민승마체험, 협력승마시설, 유소년 승마단, 학생승마체험", insrexprndt: "2020-01-01", hrareaintrcnsntnc: "복용승마장은 생활체육활성화 및 시민건강 증진을 위해 1994.8월에 건립된 대전시 유일의 공공승마장 입니다.", staffnm: "손현길"))
        }
        
        ridingGround.sort(by: {String($0.bsshAddr1) < String($1.bsshAddr1)})
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search RidingGround"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        searchCategory = ["강원", "경기", "광주", "대구", "세종", "울산", "인천"]
        searchController.searchBar.scopeButtonTitles = ["All", "강원", "경기", "광주", "대구", "세종", "울산", "인천", "기타"]
        searchController.searchBar.delegate = self

        
        /* 보이스 검색 객체 설정*/
        voiceSearch = UISearchTranscriber(button: voiceButton, searchController: searchController, tableView: tableView)
        
        tableView.reloadData()
    }

    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
          return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    func searchBarIsEmpty() -> Bool
    {
      // Returns true if the text is empty or nil
      return searchController.searchBar.text?.isEmpty ?? true
    }
      
    
    func checkEtc(ground: StardiumDetailData) -> Bool
    {
        for i in 0..<searchCategory.count{
            if String(ground.bsshAddr1).contains(searchCategory[i])
            {
                return false
            }
        }
        return true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        
      fillteredRidingGround = ridingGround.filter({( ground : StardiumDetailData) -> Bool in
        let doesCategoryMatch = (scope == "All") || (String(ground.bsshAddr1).contains(scope)) || (scope == "기타" && checkEtc(ground: ground))
          
        if searchBarIsEmpty() {
          return doesCategoryMatch
        } else {
          return doesCategoryMatch && String(ground.bsshNm).lowercased().contains(searchText.lowercased())
        }
      })
      tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if isFiltering() {
        return fillteredRidingGround.count
      }
      return ridingGround.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
      let ground: StardiumDetailData
      if isFiltering() {
        ground = fillteredRidingGround[indexPath.row]
      } else {
        ground = ridingGround[indexPath.row]
      }
        cell.textLabel!.text = String(ground.bsshNm)
        cell.detailTextLabel!.text = String(ground.bsshAddr1)
      return cell
    }
    
    // MARK: - Private instance methods
      

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      if segue.identifier == "segueToGroundDetail" {
        if let indexPath = tableView.indexPathForSelectedRow {
          //let candy = candies[indexPath.row]
          
          let ground : StardiumDetailData
          if isFiltering()
          {
            ground = fillteredRidingGround[indexPath.row]
          }
          else
          {
            ground = ridingGround[indexPath.row]
          }
          
            if let controller = (segue.destination as! UINavigationController).topViewController as? GroundHostingController
          {
                // 변수 설정
                controller.DetailData = ground
                let other = ridingGround
                ridingGround.remove(at: indexPath.row)
                controller.OtherData = other
          }
        }
      }
    }
    
    
    @IBAction func touchVoiceButton(_ sender: Any) {
        if isRecording {
            searchController.isActive = true
            voiceSearch?.stopTranscribing()
            voiceButton.setTitle("음성 검색 On", for: UIControl.State.normal)
            self.isRecording = false
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut,
                           animations: { [self] in
                            HorseImage.center =  CGPoint(x: HorseImage.center.x + 300, y: HorseImage.center.y)
                           },
                           completion: nil)

        }
        else{
            searchController.isActive = true
            voiceSearch?.startTranscribing()
            voiceButton.setTitle("음성 검색 Off", for: UIControl.State.normal)
            self.isRecording = true
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut,
                           animations: { [self] in
                            HorseImage.center =  CGPoint(x: HorseImage.center.x - 300, y: HorseImage.center.y)
                           },
                           completion: nil)
        }
        audioController.playerEffect(name: SFX_BUTTON)
    }
}

extension RidingGroundViewController: UISearchResultsUpdating {
  // MARK: - UISearchResultsUpdating Delegate
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
      let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
      filterContentForSearchText(searchController.searchBar.text!, scope: scope)
  }
}

extension RidingGroundViewController: UISearchBarDelegate {
  // MARK: - UISearchBar Delegate
  func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
  }
}
