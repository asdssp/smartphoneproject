//
//  AutoMap.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/23.
//

import Foundation
import UIKit
import MapKit
import WebKit
import CoreLocation
class AutoNaviManager : NSObject, CLLocationManagerDelegate{
    var parentView: UIView
    let regionRadius = 1000
    var start:SimpleNaviData?
    var end:SimpleNaviData?
    
    var webView:WKWebView?
    
    init(parentView: UIView){
        self.parentView = parentView
    }
    
    func LoadAutoNavi(start : SimpleNaviData?, end : SimpleNaviData){
        self.start = start
        self.end = end
        loadGoogleNavi(start: start, end: end)
       
    }
    
    func loadGoogleNavi(start: SimpleNaviData?, end:SimpleNaviData){
        if start == nil {
            let locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
            let coord = locationManager.location?.coordinate
            
            let geoCoder = CLGeocoder()
            
            if let coor = coord {
            geoCoder.reverseGeocodeLocation(CLLocation(latitude: coor.latitude, longitude: coor.longitude)) { [self] (placemarks, error) -> Void in
                    if error != nil {
                        return
                    }
                    guard let placemark = placemarks?.first,
                      let addrList = placemark.addressDictionary?["FormattedAddressLines"] as? [String] else {
                    return
                    }
                    let address = addrList.joined(separator: " ")
                    openMapSite(start: SimpleNaviData(name: address, addr: address), end: end)
                }
            }
            else{
                openMapSite(start: SimpleNaviData(name: "", addr: ""), end: end)
            }
        }
        else {
            openMapSite(start: start!, end: end)
        }
    }
    
    func openMapSite(start:SimpleNaviData, end:SimpleNaviData)
    {
        let urlOriginString = "https://www.google.com/maps/dir/" + start.name + "/" + end.name
        let urlString = urlOriginString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: urlString!) else
        {
            print("faile to load googleMap")
            return
        }
        let request = URLRequest(url : url)
    
        if webView == nil{
            webView = WKWebView(frame: CGRect(x: 0, y: 0, width: (parentView.window?.frame.width)!, height: (parentView.window?.frame.height)!))
        }
        else{
            webView?.isHidden = false
        }
        self.parentView.addSubview(webView!)
        webView?.load(request)
    }
}
