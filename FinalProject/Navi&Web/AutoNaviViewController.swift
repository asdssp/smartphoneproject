//
//  AutoMapViewController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/23.
//

import UIKit


struct SimpleNaviData{
    var name:String
    var addr:String
}

class AutoNaviViewController: ViewController {
    var simpleStartNaviData:SimpleNaviData?
    var simpleEndNaviData:SimpleNaviData?
    var autoMap: AutoNaviManager?
    
    @IBOutlet var uiView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.uiView.window != nil
        {
            autoMap = AutoNaviManager(parentView: self.uiView)
            autoMap?.LoadAutoNavi(start: simpleStartNaviData, end:simpleEndNaviData!)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        if autoMap == nil {
            autoMap = AutoNaviManager(parentView: self.uiView)
            autoMap?.LoadAutoNavi(start: simpleStartNaviData, end:simpleEndNaviData!)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
