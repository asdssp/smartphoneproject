//
//  RidingGroundViewController.swift
//  SmartPhoneTeamProject
//
//  Created by kpugame on 2021/05/20.
//

import UIKit

class WayPointViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    //출발지 입력 textFeild 링크
    @IBOutlet weak var startingPointTextFeild: UITextField!
    
    
    /* XML 관련 변수 - viewController의 prepare 함수에서 세팅 */
    // xml 주소
    var url: String?
    // xml 서비스 키
    var serviceKey: String?
    // xml 구조 내 상위 카테고리
    var HigherCategory: String?
    // xml 구조 내 카테고리들
    var Categorys: [String]?
    /* EOG */
    
    /* XML 관련 변수 - 검색기 및 검색 옵션 */
    var XMLParserManager = XMLParserClass()
    // 검색 페이지
    var pageNo: Int = 1
    // 페이지 당 결과 개수
    var numOfRows: Int = 500
    /* EOG */
    
    let searchController = UISearchController(searchResultsController: nil)
    var fillteredWayPoint = [WayPoint]()
    var wayPoint = [WayPoint]()
    
    var searchCategory = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        XMLParserManager.setHigherCategory(category: HigherCategory!)
        XMLParserManager.setCategory(categorys: Categorys!)
        
        // 파싱 시작
        XMLParserManager.beginParsing(xmlURL: url!, Key: serviceKey!, Option: "&numOfRows=" + String(numOfRows) + "&pageNo=" + String(pageNo))

        // 일부 데이터 추출
        for i in 0..<XMLParserManager.getCount(){
            let bsshNm = XMLParserManager.getCategoryData(category: "bsshNm", index: i)
            let bsshAddr1 = XMLParserManager.getCategoryData(category: "bsshAddr1", index: i)
            wayPoint.append(WayPoint(name: bsshNm as String, wayPoint: bsshAddr1 as String))
        }
        
        
        if(wayPoint.count == 0)
        {
            wayPoint.append(WayPoint(name: "대전복용승마장", wayPoint: "대전광역시 유성구 덕명로56번길 199"))
        }
        
        
        
        wayPoint.sort(by: {$0.wayPoint < $1.wayPoint})
        
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search RidingGround"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        searchCategory = ["강원", "경기", "광주", "대구", "세종", "울산", "인천"]
        searchController.searchBar.scopeButtonTitles = ["All", "강원", "경기", "광주", "대구", "세종", "울산", "인천", "기타"]
        searchController.searchBar.delegate = self

        tableView.reloadData()
    }

    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
          return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if isFiltering() {
        return fillteredWayPoint.count
      }
        
      return wayPoint.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
      let waypoint: WayPoint
      if isFiltering() {
        waypoint = fillteredWayPoint[indexPath.row]
      } else {
        waypoint = wayPoint[indexPath.row]
      }
        cell.textLabel!.text = waypoint.name
        cell.detailTextLabel!.text = waypoint.wayPoint
      return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // MARK: - Private instance methods
      
    func searchBarIsEmpty() -> Bool
    {
      // Returns true if the text is empty or nil
      return searchController.searchBar.text?.isEmpty ?? true
    }
      
    func checkEtc(waypoint: WayPoint) -> Bool
    {
        for i in 0..<searchCategory.count{
            if waypoint.wayPoint.contains(searchCategory[i])
            {
                return false
            }
        }
        return true
    }
    
    
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        fillteredWayPoint = wayPoint.filter({( waypoint : WayPoint) -> Bool in
          let doesCategoryMatch = (scope == "All") || (waypoint.wayPoint.contains(scope)) || (scope == "기타" && checkEtc(waypoint: waypoint))
            
          if searchBarIsEmpty() {
            return doesCategoryMatch
          } else {
            return doesCategoryMatch && waypoint.name.lowercased().contains(searchText.lowercased())
          }
        })
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      if segue.identifier == "segueToNaviMap" {
        if let indexPath = tableView.indexPathForSelectedRow {
          //let candy = candies[indexPath.row]
          
          let waypoint : WayPoint
          if isFiltering()
          {
            waypoint = fillteredWayPoint[indexPath.row]
          }
          else
          {
            waypoint = wayPoint[indexPath.row]
          }
         
          if let autoNaviController = segue.destination as? AutoNaviViewController {
                autoNaviController.simpleEndNaviData = SimpleNaviData(name: waypoint.name, addr: waypoint.wayPoint)
                    if  startingPointTextFeild.text != "" {
                        autoNaviController.simpleStartNaviData = SimpleNaviData(name: startingPointTextFeild.text!, addr: startingPointTextFeild.text!)
                    }
                    else{
                        autoNaviController.simpleStartNaviData = nil
                    }
                }
            }
        }
    }
}

extension WayPointViewController: UISearchResultsUpdating {
  // MARK: - UISearchResultsUpdating Delegate
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
      let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
      filterContentForSearchText(searchController.searchBar.text!, scope: scope)
  }
}

extension WayPointViewController: UISearchBarDelegate {
  // MARK: - UISearchBar Delegate
  func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
  }
}
