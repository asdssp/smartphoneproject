//
//  StartViewController.swift
//  FinalProject
//
//  Created by kpugame on 2021/05/30.
//

import UIKit

let SFX_START = "Start.mp3"

class StartViewController: UIViewController {

    var audioController: AudioController
    required init?(coder aDecoder:NSCoder){
        audioController = AudioController()
        audioController.preloadAudioEffects(audioFileNames: [SFX_START])
        super.init(coder: aDecoder)
    }


    
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var GifView: UIImageView!
    override func viewDidLoad() {
        GifView.image = UIImage.gifImageWithName("L4lW")
        super.viewDidLoad()

        let leafFX = GrassFallView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        self.view.addSubview(leafFX)
        //self.view.sendSubviewToBack(leafFX)
        self.view.bringSubviewToFront(startButton)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func TouchStartButton(_ sender: Any){
        audioController.playerEffect(name: SFX_START)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
