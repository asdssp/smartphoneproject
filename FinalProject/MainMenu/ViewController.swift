//
//  ViewController.swift
//  SmartPhoneTeamProject
//
//  Created by kpugame on 2021/05/19.
//

import UIKit


let SFX_BUTTON = "button-37a.wav"
let SFX_HORSE = "horse.mp3"
class ViewController: UIViewController, UIScrollViewDelegate {

    var pageImages: [UIImage] = []
    var pageViews: [UIImageView?] = []
    
    var audioController: AudioController
    required init?(coder aDecoder:NSCoder){
        audioController = AudioController()
        audioController.preloadAudioEffects(audioFileNames: [SFX_BUTTON, SFX_HORSE])
        super.init(coder: aDecoder)
    }
    
    @IBOutlet var mainScrollView : UIScrollView!
    @IBOutlet var mainPageControl : UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageImages = [UIImage(named: "승마장3.png")!,
                      UIImage(named: "승마장4.png")!,
                      UIImage(named: "승마장5.png")!,
                      UIImage(named: "승마장6.png")!,
                      UIImage(named: "승마장7.png")!]
        let pageCount = pageImages.count
        
        if mainScrollView != nil && mainPageControl != nil{
            mainPageControl.currentPage = 0
            mainPageControl.numberOfPages = pageCount
        
            for _ in 0..<pageCount{
                pageViews.append(nil)
            }
        
            let pagesScrollViewSize = mainScrollView.frame.size
            mainScrollView.contentSize = CGSize(width: pagesScrollViewSize.width * CGFloat(pageCount), height: pagesScrollViewSize.height)
        
            loadVisiblePages()
        }
        // Do any additional setup after loading the view.
    }
    
    

    @IBAction func StadiumButton(_ sender: Any) {
        audioController.playerEffect(name: SFX_BUTTON)
    }
    
    
    @IBAction func FindRoadButton(_ sender: Any) {
        audioController.playerEffect(name: SFX_BUTTON)
    }
    
    
    @IBAction func HorseButton(_ sender: Any) {
        audioController.playerEffect(name: SFX_HORSE)
    }
    
    @IBAction func ReviewButton(_ sender: Any) {
        audioController.playerEffect(name: SFX_BUTTON)
    }
    
    

    func loadVisiblePages(){
        let pageWidth = mainScrollView.frame.width
        let page = Int(floor((mainScrollView.contentOffset.x * 2.0 + pageWidth)/(pageWidth * 2.0)))
        
        mainPageControl.currentPage = page
        
        let firstPage = page - 1
        let lastPage = page + 1
        
        for index in 0 ..< firstPage+1{
            purgePage(index)
        }
        
        for index in firstPage ... lastPage{
            loadPage(index)
        }
        
        for index in lastPage+1 ..< pageImages.count+1{
            purgePage(index)
        }
        
    }
    
    func loadPage(_ page: Int)
    {
        if page < 0 || page >= pageImages.count {
            return
        }
        
        if pageViews[page] != nil{
            
        }
        else{
            var frame = mainScrollView.bounds
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0.0
            
            let newPageView = UIImageView(image: pageImages[page])
            newPageView.contentMode = .scaleAspectFit
            newPageView.frame = frame
            mainScrollView.addSubview(newPageView)
            
            pageViews[page] = newPageView
        }
    }

    func purgePage(_ page: Int){
        if page < 0 || page >= pageImages.count {
            return
        }
        
        if let pageView = pageViews[page]{
            pageView.removeFromSuperview()
            pageViews[page] = nil
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        loadVisiblePages()
    }
    
    
    @IBAction func doneTo(seuge:UIStoryboardSegue){
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToGround" {
            if let navController = segue.destination as? UINavigationController {
                if let topViewController = navController.topViewController as? RidingGroundViewController{
                    topViewController.url = "http://apis.data.go.kr/B551015/API31/horseRidingCourseInfo"
                    topViewController.serviceKey = "hUKJrmZlH81wgVYk3E5LdpBuXksbyw%2FD22Pl8RM2lY%2FZftVAoE2dVd1zUYZmCVeILV4jkc1D5ZgzF1My8o1xxg%3D%3D"
                    topViewController.HigherCategory = "item"
                    topViewController.Categorys = ["bizregnum", "bsshAddr1", "bsshAddr2","bsshNm","corprHrridingFcltyTyText","edcProgrmText","homepage","hrareaintrcnsntnc","hrareatynm","hresum","insrexprndt","operationTime","ptnProgrmText","reprsntcoach","rprsntvEmail","rprsntvNm","rprsntvPhone","rstde","staffnm"]
                }
            }
        }
        if segue.identifier == "segueToHorse" {
            if let navController = segue.destination as? UINavigationController {
                if let topViewController = navController.topViewController as? RidingHorseViewController{
                    topViewController.url = "http://apis.data.go.kr/B551015/API41/saddleHorseInfo"
                    topViewController.serviceKey = SearchHorseDetail_Encoding
                    topViewController.HigherCategory = "item"
                    topViewController.Categorys = ["hrName","hrNo","birthDt","sex","color","breed","pggDt","prdCty","fhrNm","mhrNm","owNm","poNm"]
                }
            }
        }
        if segue.identifier == "segueToReview" {
                    if let navController = segue.destination as? UINavigationController {
                        if let topViewController = navController.topViewController as? ReviewViewController{
                            topViewController.url = "http://apis.data.go.kr/B551015/API31/horseRidingCourseInfo"
                            topViewController.serviceKey = "hUKJrmZlH81wgVYk3E5LdpBuXksbyw%2FD22Pl8RM2lY%2FZftVAoE2dVd1zUYZmCVeILV4jkc1D5ZgzF1My8o1xxg%3D%3D"
                            topViewController.HigherCategory = "item"
                            topViewController.Categorys = ["bizregnum", "bsshAddr1", "bsshAddr2","bsshNm","corprHrridingFcltyTyText","edcProgrmText","homepage","hrareaintrcnsntnc","hrareatynm","hresum","insrexprndt","operationTime","ptnProgrmText","reprsntcoach","rprsntvEmail","rprsntvNm","rprsntvPhone","rstde","staffnm"]
                        }
                    }
                }
                
                if segue.identifier == "SegueToWayPoint" {
                    if let navController = segue.destination as? UINavigationController {
                        if let topViewController = navController.topViewController as? WayPointViewController{
                            topViewController.url = "http://apis.data.go.kr/B551015/API31/horseRidingCourseInfo"
                            topViewController.serviceKey = "hUKJrmZlH81wgVYk3E5LdpBuXksbyw%2FD22Pl8RM2lY%2FZftVAoE2dVd1zUYZmCVeILV4jkc1D5ZgzF1My8o1xxg%3D%3D"
                            topViewController.HigherCategory = "item"
                            topViewController.Categorys = ["bizregnum", "bsshAddr1", "bsshAddr2","bsshNm","corprHrridingFcltyTyText","edcProgrmText","homepage","hrareaintrcnsntnc","hrareatynm","hresum","insrexprndt","operationTime","ptnProgrmText","reprsntcoach","rprsntvEmail","rprsntvNm","rprsntvPhone","rstde","staffnm"]
                        }
                    }
                }

    }
}

